package uni.fe.tnuv.tnuv_mmshare.HelperActivityClassess;

import android.content.Context;
import android.util.Log;
import android.widget.TextView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uni.fe.tnuv.tnuv_mmshare.ContentView;
import uni.fe.tnuv.tnuv_mmshare.JsonHelperClasses.Balance;

public class HandleBalance implements Callback<Balance> {
    private final TextView money;
    public HandleBalance(Context context, TextView money){
        Context context1 = context;
        this.money = money;
    }

    @Override
    public void onResponse(Call<Balance> call, Response<Balance> response) {
        Balance balance = response.body();
        if(response.isSuccessful()){
            int money1 = balance.getBalance()-1;
            money.setText(Integer.toString(money1));
            ContentView.balance = money1;
        }else{
            Log.e("tag", "not getting user balance");
        }
    }

    @Override
    public void onFailure(Call<Balance> call, Throwable t) {
        t.printStackTrace();
    }
}
