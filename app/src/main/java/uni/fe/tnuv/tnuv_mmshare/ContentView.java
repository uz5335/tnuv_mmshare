package uni.fe.tnuv.tnuv_mmshare;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uni.fe.tnuv.tnuv_mmshare.ApplicationObjectAndStorage.ApplicationObject;
import uni.fe.tnuv.tnuv_mmshare.Fragments.HaveAccessToFileFragment;
import uni.fe.tnuv.tnuv_mmshare.Fragments.NotOwningFileFragment;
import uni.fe.tnuv.tnuv_mmshare.Fragments.OwnFileFragment;
import uni.fe.tnuv.tnuv_mmshare.HelperActivityClassess.HandleBalance;
import uni.fe.tnuv.tnuv_mmshare.HelperActivityClassess.HandleContentPurchase;
import uni.fe.tnuv.tnuv_mmshare.HelperActivityClassess.HandleDeleteContent;
import uni.fe.tnuv.tnuv_mmshare.HelperActivityClassess.HandleFileDownload;
import uni.fe.tnuv.tnuv_mmshare.HelperActivityClassess.HandleUser;
import uni.fe.tnuv.tnuv_mmshare.JsonHelperClasses.Content;
import uni.fe.tnuv.tnuv_mmshare.PermisionsUtils.NetworkInfoClass;
import uni.fe.tnuv.tnuv_mmshare.Utils.CheckAccess;
import uni.fe.tnuv.tnuv_mmshare.Utils.IntentUtils;
import uni.fe.tnuv.tnuv_mmshare.interfaces.ContentInterface;
import uni.fe.tnuv.tnuv_mmshare.interfaces.UserInterface;

public class ContentView extends AppCompatActivity implements Callback<Content> {

    private static String contentId;
    public static int balance;
    private static String contentName;
    private static final int PERMISSIONS_REQUEST_ACCESS_EXTERNALSTORAGE= 1;
    private boolean externalStorageGranted;

    private TextView fileName;
    private TextView topic;
    private TextView dateCreated;
    private TextView uploaderName;
    private TextView fileType;
    private  TextView message;
    private ProgressBar progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_view);

        setTitle("Details");

        Toolbar toolbar = findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        Objects.requireNonNull(toolbar.getNavigationIcon()).setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);






        /*
        Toolbar app_bar = findViewById(R.id.app_bar);
        setSupportActionBar(app_bar);
        */

        NetworkInfo netInfo = NetworkInfoClass.getActiveNetworkInfo(getApplicationContext());
        if(netInfo == null || !netInfo.isConnected()){ //preverjanje ali sem kam povezan...
            Toast.makeText(ContentView.this, "NO INTERNET CONNECTION", Toast.LENGTH_SHORT).show();
            final ApplicationObject appObject = (ApplicationObject) getApplication();
            appObject.deleteUserId();
            IntentUtils.StartNewActivity(getApplicationContext(), Login.class);
            return;
        }
        checkLogedInUser();
        TextView userName = findViewById(R.id.userName1);
        TextView money = findViewById(R.id.money1);
        fileName = findViewById(R.id.fileNameOne);
        topic = findViewById(R.id.theme);
        dateCreated = findViewById(R.id.date);
        uploaderName = findViewById(R.id.uploader);
        fileType = findViewById(R.id.fileType);
        message = findViewById(R.id.textMessage);
        progress = findViewById(R.id.progressBar2);
        progress.setVisibility(View.GONE);
        externalStorageGranted = false;
        getExternalStoragePermision();

        UserInterface.getInstance().GetOneUser(getUserId()).enqueue(new HandleUser(getApplicationContext(), userName));
        UserInterface.getInstance().GetUserBalance(getUserId()).enqueue(new HandleBalance(getApplicationContext(), money));

        final String id = getIntent().getStringExtra(getResources().getString(R.string.fielName));
        if (!id.equals("")) {
            Log.i("tag", id);
            ContentInterface.getInstance().getOneContent(id).enqueue(this);
        }else{
            Log.e("tag", "Something went terible wrong");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkLogedInUser();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        checkLogedInUser();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkLogedInUser();
    }
    private void  checkLogedInUser(){
        final ApplicationObject app = (ApplicationObject) getApplication();
        if(app.getUserID() == null){
            IntentUtils.StartNewActivity(getApplicationContext(), Login.class);
        }
    }

    private void getExternalStoragePermision() {

        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            externalStorageGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSIONS_REQUEST_ACCESS_EXTERNALSTORAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        externalStorageGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_EXTERNALSTORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    externalStorageGranted = true;
                }
            }
        }
    }

    private String getUserId(){
        final ApplicationObject app = (ApplicationObject) getApplication();
        return app.getUserID();
    }

    /*

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        for(int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            SpannableString spanString = new SpannableString(menu.getItem(i).getTitle().toString());
            int end = spanString.length();
            spanString.setSpan(new RelativeSizeSpan(1.3f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            item.setTitle(spanString);
        }

        return true;
    }

    */

    public void showToast(String besedilo){

        Context context = getApplicationContext();
        String text =  besedilo; //getResources().getString(R.string.toast);
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

    /*

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.HOME:
                //showToast("going home"); //ukaz za pridobitev referenca hardcodana besedila.... v aplikaciji.
                IntentUtils.StartNewActivity(getApplicationContext(), Home.class);
                return true;
            case R.id.MYFILES:
                //showToast("My files");
                IntentUtils.StartNewActivity(getApplicationContext(), MyFiles.class);
                return true;
            case R.id.THEMES:
                //showToast("themes");
                IntentUtils.StartNewActivity(getApplicationContext(), Topic_list.class);
                return true;

            case R.id.LOGOUT:
                //showToast("logout");
                final ApplicationObject appObject = (ApplicationObject) getApplication();
                appObject.deleteUserId();
                PresheredStorageClass.deleteFromStorage(getApplicationContext());
                IntentUtils.StartNewActivity(getApplicationContext(), Login.class);
                return true;
            case R.id.DOWNLOADS:
                DownloadsUtils.openDownloads(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here

                //Intent intent = new Intent(CurrentActivity.this, MainActivity.class);
                //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                //startActivity(intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResponse(Call<Content> call, Response<Content> response) {
        Content content = response.body();
        if(response.isSuccessful()){
            Log.e("tag",Integer.toString(balance));

            if(content.getCreator().getId().equals(getUserId())){
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.placeholder, new OwnFileFragment());
                ft.commit();
                message.setText(getResources().getString(R.string.creatorMessage));
            }else if (CheckAccess.HaveAccessToFile(content.getAccess(), getUserId())){

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.placeholder, new HaveAccessToFileFragment());
                ft.commit();
                message.setText(getResources().getString(R.string.accessMessage));
            }else{
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.placeholder, new NotOwningFileFragment(content.getPrice(), balance));
                ft.commit();
                message.setText(getResources().getString(R.string.notOwningFile));
            }

            contentId = content.getId();
            contentName = content.getFileName();
            fileName.setText(content.getFileName());
            topic.setText(content.getTopicID().getName());
            uploaderName.setText(content.getCreator().getUsername());
            fileType.setText(content.getMimetype());

            String pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS";

            // since no built-in format, we provides pattern directly.
            DateFormat df = new SimpleDateFormat(pattern);

            try {
                Date myDate = df.parse(content.getTimeCreated());
                dateCreated.setText("" + myDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }else{
            Log.e("tag", "Can't find this content");
        }
    }

    @Override
    public void onFailure(Call<Content> call, Throwable t) {
        t.printStackTrace();
    }

    //odzivna funkcija za prenos datoteke iz serverja....
    public void Download(View v){
        Log.i("TAG", "on click za download");
        getExternalStoragePermision();
        if(externalStorageGranted){
            progress.setVisibility(View.VISIBLE);
            HandleFileDownload download = new HandleFileDownload(getApplicationContext(), contentName, progress);
            ContentInterface.getInstance().downloadFile(getUserId(), contentId).enqueue(download);

        }else{
            Log.d("tag", "can't write to external storage");
            Toast toast = Toast.makeText(ContentView.this, "Permission for external storage not granted", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public void RemoveContent(View v){
        Log.i("tag", "lol");


        AlertDialog.Builder builder = new AlertDialog.Builder(ContentView.this);
        builder.setTitle("Do you really want to delete this file?");

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                HandleDeleteContent delete = new HandleDeleteContent(getApplicationContext());
                ContentInterface.getInstance().DeleteOneContent(getUserId(), contentId).enqueue(delete);
                Toast toast = Toast.makeText(ContentView.this, "File deleted", Toast.LENGTH_SHORT);
                toast.show();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d("tag", "deletion was canceled");
                Toast toast = Toast.makeText(ContentView.this, "Deletion cancelled", Toast.LENGTH_SHORT);
                toast.show();
            }
        });
        //create dialog box...to show it..
        AlertDialog ad = builder.create();
        ad.show();
    }

    public void Purchase(View v){
        Log.i("tag", "lol");
        AlertDialog.Builder builder = new AlertDialog.Builder(ContentView.this);
        builder.setTitle("Please confirm your purchase");
        builder.setIcon(R.mipmap.favicon);
        builder.setMessage("Enter your password");
        final EditText input = new EditText(ContentView.this);
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        builder.setView(input);
        builder.setPositiveButton("Purchase", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String password = input.getText().toString();
                Log.d("tag", password);
                HandleContentPurchase purchaseContent = new HandleContentPurchase(getApplicationContext());
                ContentInterface.getInstance().PurchaseContent(getUserId(), contentId, password).enqueue(purchaseContent);
                Toast toast = Toast.makeText(ContentView.this, "Purchase successful", Toast.LENGTH_SHORT);
                toast.show();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d("tag", "purchase was canceled");
            }
        });
        //create dialog box...to show it..
        AlertDialog ad = builder.create();
        ad.show();


    }
}
