package uni.fe.tnuv.tnuv_mmshare.Fragments;

import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import uni.fe.tnuv.tnuv_mmshare.R;

@SuppressLint("ValidFragment")
public class NotOwningFileFragment extends Fragment {

    private final int price1;
    private final int balance;
    @SuppressLint("ValidFragment")
    public NotOwningFileFragment(int price1, int balance) {
        // Required empty public constructor
        this.price1 = price1;
        this.balance = balance;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        View view = inflater.inflate(R.layout.fragment_not_owning_file, parent, false);
        TextView price = view.findViewById(R.id.contentPrice);
        FloatingActionButton purchase = view.findViewById(R.id.purchaseFAB);

        if(price1 > balance){
            purchase.setEnabled(false);
            purchase.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.disablecolor)));
        }else{
            purchase.setEnabled(true);
            purchase.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.barva)));
        }

        price.setText(""+ Integer.toString(price1));

        return view;
    }

    // This event is triggered soon after onCreateView().
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        // Setup any handles to view objects here
        // EditText etFoo = (EditText) view.findViewById(R.id.etFoo);
        //
    }
}
