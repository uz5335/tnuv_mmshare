package uni.fe.tnuv.tnuv_mmshare.HelperActivityClassess;


import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uni.fe.tnuv.tnuv_mmshare.ApplicationObjectAndStorage.ApplicationObject;
import uni.fe.tnuv.tnuv_mmshare.ApplicationObjectAndStorage.PresheredStorageClass;
import uni.fe.tnuv.tnuv_mmshare.Login;
import uni.fe.tnuv.tnuv_mmshare.R;

public class HandleFileDownload implements Callback<ResponseBody> {
    private final Context context;
    private final String fileName;
    private final ProgressBar progress;
    public HandleFileDownload(Context context, String fileName, ProgressBar progress){
        this.context = context;
        this.fileName = fileName;
        this.progress = progress;
    }

    @Override
    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        //Log.d("TAG", "notri....");
        if(response.isSuccessful()){
            try {
                downloadFile(response.body());

            } catch (IOException e) {

                e.printStackTrace();
                //Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
            }
        }else{
            Log.d("tag", "NAROBE GRE PRI PRENOSU...");
            Toast.makeText(this.context,this.context.getResources().getString(R.string.errorService), Toast.LENGTH_SHORT).show();
            final ApplicationObject appObject = (ApplicationObject) this.context;
            appObject.deleteUserId();
            PresheredStorageClass.deleteFromStorage(this.context);
            Intent intent = new Intent(this.context, Login.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.context.startActivity(intent);
        }
    }

    @Override
    public void onFailure(Call<ResponseBody> call, Throwable t) {
        //Log.w(TAG, "Error: " + t.getMessage(), t);
        //container.setRefreshing(false);
        Log.v("TAG", "something went worng");
    }



    private void downloadFile(ResponseBody body) throws IOException {
        Log.d("TAG", "WRITTING........");
        int count;
        byte data[] = new byte[1024 * 4];
        long fileSize = body.contentLength();
        InputStream bis = new BufferedInputStream(body.byteStream(), 1024 * 8);
        File outputFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), this.fileName);
        Log.d("tag", outputFile.getAbsolutePath());
        OutputStream output = new FileOutputStream(outputFile);
        long total = 0;
        while ((count = bis.read(data)) != -1) {
            total += count;
            Log.d("TAG", "writting"+total);
            output.write(data, 0, count);
        }

        output.flush();
        output.close();
        bis.close();
        this.progress.setVisibility(View.GONE);
        Toast toast = Toast.makeText(this.context, this.context.getResources().getString(R.string.fileDownloadComplete), Toast.LENGTH_SHORT);
        toast.show();

    }

}
