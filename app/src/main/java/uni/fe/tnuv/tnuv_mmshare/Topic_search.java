package uni.fe.tnuv.tnuv_mmshare;

import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uni.fe.tnuv.tnuv_mmshare.ApplicationObjectAndStorage.ApplicationObject;
import uni.fe.tnuv.tnuv_mmshare.ApplicationObjectAndStorage.PresheredStorageClass;
import uni.fe.tnuv.tnuv_mmshare.HelperActivityClassess.HandleBalance;
import uni.fe.tnuv.tnuv_mmshare.HelperActivityClassess.HandleUser;
import uni.fe.tnuv.tnuv_mmshare.JsonHelperClasses.Content;
import uni.fe.tnuv.tnuv_mmshare.PermisionsUtils.NetworkInfoClass;
import uni.fe.tnuv.tnuv_mmshare.Utils.DownloadsUtils;
import uni.fe.tnuv.tnuv_mmshare.Utils.IntentUtils;
import uni.fe.tnuv.tnuv_mmshare.Utils.ListContentAdapter;
import uni.fe.tnuv.tnuv_mmshare.interfaces.SearchInterface;
import uni.fe.tnuv.tnuv_mmshare.interfaces.UserInterface;

public class Topic_search extends AppCompatActivity implements Callback<List<Content>> {

    private ListContentAdapter adapter;
    private SwipeRefreshLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic_search);

        NetworkInfo netInfo = NetworkInfoClass.getActiveNetworkInfo(getApplicationContext());
        if(netInfo == null || !netInfo.isConnected()){ //preverjanje ali sem kam povezan...
            Toast.makeText(Topic_search.this, "NO INTERNET CONNECTION", Toast.LENGTH_SHORT).show();
            final ApplicationObject appObject = (ApplicationObject) getApplication();
            appObject.deleteUserId();
            IntentUtils.StartNewActivity(getApplicationContext(), Login.class);
            return;
        }
        checkLogedInUser();
        ListView list = findViewById(R.id.items2);
        adapter = new ListContentAdapter(getApplicationContext());

        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final Content content = adapter.getItem(i);
                if (content != null) {
                    Log.d("tag", content.getFileName());
                    final Intent intent = new Intent(Topic_search.this, ContentView.class);
                    intent.putExtra(getResources().getString(R.string.fielName), content.getId());
                    intent.putExtra("this_activity_sent_me", this.getClass().getSimpleName());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }
        });

        TextView userName = findViewById(R.id.userName);
        TextView money = findViewById(R.id.money);
        TextView topic123 = findViewById(R.id.topicName2);

        container = findViewById(R.id.container2);

        UserInterface.getInstance().GetOneUser(getUserId()).enqueue(new HandleUser(getApplicationContext(), userName));
        UserInterface.getInstance().GetUserBalance(getUserId()).enqueue(new HandleBalance(getApplicationContext(), money));

        //Iz intenta preberi za katero temo se gre in na podlagi tega naredi poizvedbo po temi in naredi prikaz..
        final String name = getIntent().getStringExtra(getResources().getString(R.string.topicName123));
        setTitle(name);
        if (!name.equals("")) {
            Log.e("tag", name);
            //ContentInterface.getInstance().getOneContent(id).enqueue(this);
            topic123.setText(getResources().getString(R.string.topicName) + " "+name);
            container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    SearchInterface.getInstance().searchTopics(name).enqueue(Topic_search.this);
                }
            });
            SearchInterface.getInstance().searchTopics(name).enqueue(Topic_search.this);
        }else{
            Log.e("tag", "Something went terible wrong");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkLogedInUser();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        checkLogedInUser();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkLogedInUser();
    }

    private void  checkLogedInUser(){
        final ApplicationObject app = (ApplicationObject) getApplication();
        if(app.getUserID() == null){
            IntentUtils.StartNewActivity(getApplicationContext(), Login.class);
        }
    }

    private String getUserId(){
        final ApplicationObject app = (ApplicationObject) getApplication();
        return app.getUserID();
    }

    /*

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        for(int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            SpannableString spanString = new SpannableString(menu.getItem(i).getTitle().toString());
            int end = spanString.length();
            spanString.setSpan(new RelativeSizeSpan(1.3f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            item.setTitle(spanString);
        }

        return true;
    }

    */

    public void showToast(String besedilo){

        Context context = getApplicationContext();
        String text =  besedilo; //getResources().getString(R.string.toast);
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.HOME:
                //showToast("going home"); //ukaz za pridobitev referenca hardcodana besedila.... v aplikaciji.
                IntentUtils.StartNewActivity(getApplicationContext(), Home.class);
                return true;
            case R.id.MYFILES:
                //showToast("My files");
                IntentUtils.StartNewActivity(getApplicationContext(), MyFiles.class);
                return true;
            case R.id.THEMES:
                //showToast("themes");
                IntentUtils.StartNewActivity(getApplicationContext(), Topic_list.class);
                return true;
            case R.id.LOGOUT:
                //showToast("logout");
                final ApplicationObject appObject = (ApplicationObject) getApplication();
                appObject.deleteUserId();
                PresheredStorageClass.deleteFromStorage(getApplicationContext());
                IntentUtils.StartNewActivity(getApplicationContext(), Login.class);
                return true;
            case R.id.DOWNLOADS:
                DownloadsUtils.openDownloads(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResponse(Call<List<Content>> call, Response<List<Content>> response) {
        List<Content> contents = response.body();
        if(response.isSuccessful()){
            adapter.clear();
            adapter.addAll(contents);
        }else{
            Log.e("tag", "reqest din't end with 200OK");
            Toast.makeText(this,getResources().getString(R.string.errorService), Toast.LENGTH_SHORT).show();
            final ApplicationObject appObject = (ApplicationObject) getApplication();
            appObject.deleteUserId();
            PresheredStorageClass.deleteFromStorage(getApplicationContext());
            IntentUtils.StartNewActivity(getApplicationContext(), Login.class);
        }
        container.setRefreshing(false);
    }

    @Override
    public void onFailure(Call<List<Content>> call, Throwable t) {
        t.printStackTrace();
        container.setRefreshing(false);
    }
}
