package uni.fe.tnuv.tnuv_mmshare;

import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;

import uni.fe.tnuv.tnuv_mmshare.ApplicationObjectAndStorage.ApplicationObject;
import uni.fe.tnuv.tnuv_mmshare.ApplicationObjectAndStorage.PresheredStorageClass;
import uni.fe.tnuv.tnuv_mmshare.Utils.IntentUtils;

public class SplashScreen extends AppCompatActivity {
    private static final int SPLASH_TIME_OUT = 3000;
    private ProgressBar progressBar;

    public static final String PREFS = "PREFS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setScaleY(3f);
        progressBar.setMax(100);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            progressBar.setProgressTintList(ColorStateList.valueOf(getResources().getColor(R.color.barva)));
        } else {
            Drawable progressDrawable = progressBar.getProgressDrawable().mutate();
            progressDrawable.setColorFilter(getResources().getColor(R.color.barva), android.graphics.PorterDuff.Mode.SRC_IN);
            progressBar.setProgressDrawable(progressDrawable);
        }

        final ProgressBarAsync task = new ProgressBarAsync();
        task.execute(3);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!PresheredStorageClass.getFromStorage(getApplicationContext()).equals("")) {
                    String userId = PresheredStorageClass.getFromStorage(getApplicationContext());
                    final ApplicationObject appObject = (ApplicationObject) getApplication();
                    appObject.setUserID(userId);
                    IntentUtils.StartNewActivity(getApplicationContext(), Home.class);
                } else {
                    IntentUtils.StartNewActivity(getApplicationContext(), Login.class);
                }
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    private class ProgressBarAsync extends AsyncTask<Integer, Double, Void> {
        @Override
        protected Void doInBackground(Integer... params) {
            for (int i = 0; i < params[0]; i++) {
                simulateWork();
                final double progress = (i + 1d) / params[0] * 100;
                publishProgress(progress);
            }

            return null;
        }

        private void simulateWork() {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ignored) {

            }
        }

        @Override
        protected void onProgressUpdate(Double... values) {
            super.onProgressUpdate(values);
            progressBar.setProgress(values[0].intValue());
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

        }
    }
}
