package uni.fe.tnuv.tnuv_mmshare.ApplicationObjectAndStorage;

import android.app.Application;

public class ApplicationObject extends Application {
    private String userID;

    public void setUserID(String userId){
        this.userID = userId;
    }

    public String getUserID(){
        return this.userID;
    }

    public void deleteUserId(){
        this.userID = null;
    }
}
