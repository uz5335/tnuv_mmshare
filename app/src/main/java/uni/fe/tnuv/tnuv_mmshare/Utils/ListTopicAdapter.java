package uni.fe.tnuv.tnuv_mmshare.Utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Objects;

import uni.fe.tnuv.tnuv_mmshare.JsonHelperClasses.Topics;
import uni.fe.tnuv.tnuv_mmshare.R;

public class ListTopicAdapter extends ArrayAdapter<Topics> {

    private final Context context;
    public ListTopicAdapter(Context context) {
        super(context, 0, new ArrayList<Topics>());
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        final Topics topics = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_element_topic, parent, false);
        }
        final TextView topicName = convertView.findViewById(R.id.topicName);
        topicName.setText(Objects.requireNonNull(topics).getName());


        return convertView;
    }

}
