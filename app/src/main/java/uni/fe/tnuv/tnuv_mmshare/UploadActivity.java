package uni.fe.tnuv.tnuv_mmshare;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import uni.fe.tnuv.tnuv_mmshare.ApplicationObjectAndStorage.ApplicationObject;
import uni.fe.tnuv.tnuv_mmshare.PermisionsUtils.NetworkInfoClass;
import uni.fe.tnuv.tnuv_mmshare.Utils.IntentUtils;
import uni.fe.tnuv.tnuv_mmshare.interfaces.ContentInterface;
import uni.fe.tnuv.tnuv_mmshare.interfaces.SearchInterface;
import uni.fe.tnuv.tnuv_mmshare.HelperActivityClassess.HandleSpinnerResult;
import uni.fe.tnuv.tnuv_mmshare.HelperActivityClassess.UploadFile;

public class UploadActivity extends AppCompatActivity {
    private ImageView mImageView;
    private Spinner spinnerTopics;
    private Button uploadButton;
    private static final int REQUEST_TAKE_PHOTO = 1;
    private ProgressBar progress;
    private String mCurrentPhotoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);

        setTitle("Upload a picture");

        Toolbar toolbar = findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        Objects.requireNonNull(toolbar.getNavigationIcon()).setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);

        NetworkInfo netInfo = NetworkInfoClass.getActiveNetworkInfo(getApplicationContext());
        if(netInfo == null || !netInfo.isConnected()){ //preverjanje ali sem kam povezan...
            Toast.makeText(UploadActivity.this, "NO INTERNET CONNECTION", Toast.LENGTH_SHORT).show();
            final ApplicationObject appObject = (ApplicationObject) getApplication();
            appObject.deleteUserId();
            IntentUtils.StartNewActivity(getApplicationContext(), Login.class);
            return;
        }
        checkLogedInUser();
        Button gumbSlika = findViewById(R.id.gumbSlika);
        mImageView = findViewById(R.id.imageView);
        gumbSlika.setOnClickListener(slika);
        spinnerTopics = findViewById(R.id.spinnerTopics);
        uploadButton = findViewById(R.id.uploadButton);
        uploadButton.setOnClickListener(Upload);
        mCurrentPhotoPath = "";
        progress = findViewById(R.id.progressBar3);
        progress.setVisibility(View.GONE);
        //Log.i("TAF", getApplicationContext().getPackageName());
        HandleSpinnerResult spinnerResult = new HandleSpinnerResult(spinnerTopics, getApplicationContext());
        SearchInterface.getInstance().allTopics().enqueue(spinnerResult);

    }

    @Override
    protected void onStart() {
        super.onStart();
        checkLogedInUser();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        checkLogedInUser();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkLogedInUser();
    }

    private void  checkLogedInUser(){
        final ApplicationObject app = (ApplicationObject) getApplication();
        if(app.getUserID() == null){
            IntentUtils.StartNewActivity(getApplicationContext(), Login.class);
        }
    }

    private final View.OnClickListener Upload = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String selectedTopic = spinnerTopics.getSelectedItem().toString();
            if(Objects.equals(selectedTopic, "Please select a topic") || Objects.equals(mCurrentPhotoPath, "")){
                Log.e("tag","NO TOPIC SELECTED OR PHOTO TAKEN");
                Toast toast = Toast.makeText(UploadActivity.this, getResources().getString(R.string.errorUpload), Toast.LENGTH_SHORT);
                toast.show();

            }else{
                Log.d("tag", "SELECTED: "+ selectedTopic);
                Log.d("TAG", mCurrentPhotoPath);
                uploadButton.setEnabled(false);
                uploadButton.setBackgroundColor(getResources().getColor(R.color.disablecolor));
                File file = new File(mCurrentPhotoPath);
                final ApplicationObject app = (ApplicationObject) getApplication();

                RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), reqFile);
                RequestBody topicName = RequestBody.create(MediaType.parse("text/plain"), selectedTopic);
                RequestBody userID = RequestBody.create(MediaType.parse("text/plain"), app.getUserID());
                progress.setVisibility(View.VISIBLE);
                UploadFile fileUpload = new UploadFile(getApplicationContext(), progress);
                ContentInterface.getInstance().uploadFiles(body, userID, topicName).enqueue(fileUpload);

            }

        }
    };

    private final View.OnClickListener slika = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            dispatchTakePictureIntent();
        }
    };

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            setPic();
            galleryAddPic();
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider1",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    private void setPic() {
        // Get the dimensions of the View
        Log.d("TAG", mCurrentPhotoPath);
        int targetW = mImageView.getWidth();
        int targetH = mImageView.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        mImageView.setImageBitmap(bitmap);
    }


    public void showToast(String besedilo){

        Context context = getApplicationContext();
        String text =  besedilo; //getResources().getString(R.string.toast);
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }



}
