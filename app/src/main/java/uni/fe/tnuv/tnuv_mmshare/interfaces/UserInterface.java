package uni.fe.tnuv.tnuv_mmshare.interfaces;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import uni.fe.tnuv.tnuv_mmshare.JsonHelperClasses.Balance;
import uni.fe.tnuv.tnuv_mmshare.JsonHelperClasses.User;

public class UserInterface {

    public interface RestApi {
        String URL = "https://diploma-denar-uz5335.c9users.io/logicAPI/"; //da pridemo v locahost od windowsa...

        @FormUrlEncoded
        @POST("login")
        Call<User> LoginCheck(@Field("username") String username,
                              @Field("password") String password);

        @GET("getUser/{userId}")
        Call<User> GetOneUser (@Path("userId") String userId);

        @GET("getUserBalance/{userId}")
        Call<Balance> GetUserBalance (@Path("userId") String userId);

    }

    private static RestApi instance;

    public static synchronized RestApi getInstance() {
        if (instance == null) {
            final Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(RestApi.URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            instance = retrofit.create(RestApi.class);
        }

        return instance;
    }

}
