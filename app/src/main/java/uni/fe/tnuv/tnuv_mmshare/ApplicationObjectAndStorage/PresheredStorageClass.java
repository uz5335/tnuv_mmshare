package uni.fe.tnuv.tnuv_mmshare.ApplicationObjectAndStorage;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import uni.fe.tnuv.tnuv_mmshare.R;

public class PresheredStorageClass {

    public static void saveToStorage(Context context, String userID){
        SharedPreferences sharedPreference =  PreferenceManager.getDefaultSharedPreferences(context);
        String kljuc = context.getResources().getString(R.string.USERID);

        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.putString(kljuc, userID);
        editor.apply();
    }

    public static String getFromStorage(Context context){
        SharedPreferences sharedPreference =  PreferenceManager.getDefaultSharedPreferences(context);
        String kljuc = context.getResources().getString(R.string.USERID);

        return sharedPreference.getString(kljuc, "");
    }

    public static  void deleteFromStorage(Context context){
        SharedPreferences sharedPreference =  PreferenceManager.getDefaultSharedPreferences(context);
        String kljuc = context.getResources().getString(R.string.USERID);
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.remove(kljuc);
        editor.apply();
    }
}
