package uni.fe.tnuv.tnuv_mmshare.interfaces;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import uni.fe.tnuv.tnuv_mmshare.JsonHelperClasses.Content;

public class ContentInterface {

    public interface RestApi {
        String URL = "https://diploma-denar-uz5335.c9users.io/logicAPI/"; //da pridemo v locahost od windowsa...

        @GET("fileDownload/{userId}/{contentId}")
        Call<ResponseBody> downloadFile (@Path("userId") String userId,
                                         @Path("contentId") String contentId);


        @Multipart
        @POST("fileUpload")
        Call<ResponseBody> uploadFiles(
                @Part MultipartBody.Part file,
                @Part("userID") RequestBody userID,
                @Part("topicName") RequestBody topicName
        );

        @GET("findContentById/{contentId}")
        Call<Content> getOneContent (@Path("contentId") String contentId);

        @FormUrlEncoded
        @HTTP(method = "DELETE", path = "deleteContent", hasBody = true)
        Call<ResponseBody> DeleteOneContent(@Field("userId") String userId,
                                            @Field("contentId") String contentId);

        @FormUrlEncoded
        @POST("buyContent")
        Call<ResponseBody> PurchaseContent(@Field("userId") String userId,
                                            @Field("contentId") String contentId,
                                           @Field("password") String password
                                           );
    }

    private static RestApi instance;

    public static synchronized RestApi getInstance() {
        if (instance == null) {
            final Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(RestApi.URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            instance = retrofit.create(RestApi.class);
        }

        return instance;
    }

}

