package uni.fe.tnuv.tnuv_mmshare.HelperActivityClassess;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uni.fe.tnuv.tnuv_mmshare.ApplicationObjectAndStorage.ApplicationObject;
import uni.fe.tnuv.tnuv_mmshare.ApplicationObjectAndStorage.PresheredStorageClass;
import uni.fe.tnuv.tnuv_mmshare.JsonHelperClasses.Topics;
import uni.fe.tnuv.tnuv_mmshare.Login;
import uni.fe.tnuv.tnuv_mmshare.R;

public class HandleSpinnerResult implements Callback<List<Topics>> {
    private final ArrayList<String> topicNames;
    private final Spinner spinnerTopics;
    private final Context context;

    public HandleSpinnerResult(Spinner spinnerTopics,  Context context){
        topicNames = new ArrayList<>();
        topicNames.add("Please select a topic");
        this.spinnerTopics = spinnerTopics;
        this.context = context;
    }

    @Override
    public void onResponse(Call<List<Topics>> call, Response<List<Topics>> response) {
        if(response.isSuccessful()){
            List <Topics> topics = response.body();
            Log.d("TAG123", String.valueOf(topics.size()));
            for(int i = 0; i < topics.size(); i++){
                Log.d("TAG123", topics.get(i).getName());
                topicNames.add(topics.get(i).getName());
            }
            this.spinnerTopics.setAdapter(new ArrayAdapter<>(this.context, R.layout.spinner_item, topicNames));

        }else{
            Log.e("error", "JOJOOJOOJJOJ");
            Toast.makeText(this.context,this.context.getResources().getString(R.string.errorService), Toast.LENGTH_SHORT).show();
            final ApplicationObject appObject = (ApplicationObject) this.context;
            appObject.deleteUserId();
            PresheredStorageClass.deleteFromStorage(this.context);
            Intent intent = new Intent(this.context, Login.class);
            this.context.startActivity(intent);

        }
    }

    @Override
    public void onFailure(Call<List<Topics>> call, Throwable t) {
        Log.e("error", "something bad happened");
        t.printStackTrace();
    }
}