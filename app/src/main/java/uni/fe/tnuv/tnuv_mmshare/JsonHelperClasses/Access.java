package uni.fe.tnuv.tnuv_mmshare.JsonHelperClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Access {

    @SerializedName("timeAssigned")
    @Expose
    private String timeAssigned;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("userID")
    @Expose
    private String userID;

    public String getTimeAssigned() {
        return timeAssigned;
    }

    public void setTimeAssigned(String timeAssigned) {
        this.timeAssigned = timeAssigned;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

}
