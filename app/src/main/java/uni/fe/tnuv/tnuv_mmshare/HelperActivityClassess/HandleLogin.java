package uni.fe.tnuv.tnuv_mmshare.HelperActivityClassess;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uni.fe.tnuv.tnuv_mmshare.ApplicationObjectAndStorage.PresheredStorageClass;
import uni.fe.tnuv.tnuv_mmshare.Home;
import uni.fe.tnuv.tnuv_mmshare.JsonHelperClasses.User;
import uni.fe.tnuv.tnuv_mmshare.ApplicationObjectAndStorage.ApplicationObject;
import uni.fe.tnuv.tnuv_mmshare.R;

public class HandleLogin implements Callback<User> {
    private final Context context;

    private final EditText password;
    private final EditText username;
    public HandleLogin(Context context, EditText password, EditText username){
        this.context = context;
        this.password = password;
        this.username = username;
    }

    @Override
    public void onResponse(Call<User> call, Response<User> response) {

        if (response.isSuccessful()) {
            final User user= response.body();
            Log.i("TAG", "jej");
            Log.i("tag", user.getId());
            this.saveToAppObject(user.getId());
            PresheredStorageClass.saveToStorage(this.context, user.getId());
            Intent intent = new Intent(this.context, Home.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.context.startActivity(intent);
        } else {
            JSONObject errorMessage;
            try {
                errorMessage = new JSONObject(response.errorBody().string());
                Toast.makeText(this.context, errorMessage.getString("error"), Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                //errorMessage = "An error occurred: error while decoding the error message.";
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(this.context,this.context.getResources().getString(R.string.errorService), Toast.LENGTH_SHORT).show();
            }
            password.setText("");
            username.setText("");
        }
    }

    @Override
    public void onFailure(Call<User> call, Throwable t) {

        Log.v("TAG", "something went worng");

    }

    private void saveToAppObject(String userID) {
        Log.i("tag", userID);
        final ApplicationObject appObject = (ApplicationObject) this.context;
        appObject.setUserID(userID);
    }
}
