package uni.fe.tnuv.tnuv_mmshare.Utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import uni.fe.tnuv.tnuv_mmshare.ApplicationObjectAndStorage.ApplicationObject;
import uni.fe.tnuv.tnuv_mmshare.JsonHelperClasses.Access;
import uni.fe.tnuv.tnuv_mmshare.JsonHelperClasses.Content;
import uni.fe.tnuv.tnuv_mmshare.R;

public class ListContentAdapter extends ArrayAdapter<Content> {
    private final Context context;
    public ListContentAdapter(Context context) {
        super(context, 0, new ArrayList<Content>());
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        final Content content = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_element, parent, false);
        }
        final TextView fileName = convertView.findViewById(R.id.fileName);
        final TextView topicName = convertView.findViewById(R.id.topicName);
        final ImageView image = convertView.findViewById(R.id.canDownloadImage);

        fileName.setText(Objects.requireNonNull(content).getFileName());
        topicName.setText(content.getTopicID().getName());

        List<Access> access = content.getAccess();
        final ApplicationObject app = (ApplicationObject) this.context;
        String userId = app.getUserID();
        Log.d("tag", userId);

        if(CheckAccess.HaveAccessToFile(content.getAccess(), userId)){
            image.setVisibility(View.VISIBLE);
            Log.d("tag", "VIDNO VIDNO VIDNO");
        }else{
            image.setVisibility(View.INVISIBLE);
            Log.d("tag", "NEEEEEEEEEEE");
        }


        return convertView;
    }

}
