package uni.fe.tnuv.tnuv_mmshare.HelperActivityClassess;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uni.fe.tnuv.tnuv_mmshare.ApplicationObjectAndStorage.ApplicationObject;
import uni.fe.tnuv.tnuv_mmshare.ApplicationObjectAndStorage.PresheredStorageClass;
import uni.fe.tnuv.tnuv_mmshare.JsonHelperClasses.User;
import uni.fe.tnuv.tnuv_mmshare.Login;
import uni.fe.tnuv.tnuv_mmshare.R;

public class HandleUser implements Callback<User> {
    private final Context context;
    private final TextView username;
    public HandleUser(Context context, TextView username){
        this.context = context;
        this.username = username;
    }

    @Override
    public void onResponse(Call<User> call, Response<User> response) {
        User user = response.body();
        if(response.isSuccessful()){
            this.username.setText(user.getUsername());
        }else {
            Log.e("tag", "no user found");
            Toast.makeText(this.context,this.context.getResources().getString(R.string.errorService), Toast.LENGTH_SHORT).show();
            final ApplicationObject appObject = (ApplicationObject) this.context;
            appObject.deleteUserId();
            PresheredStorageClass.deleteFromStorage(this.context);
            Intent intent = new Intent(this.context, Login.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.context.startActivity(intent);
        }
    }

    @Override
    public void onFailure(Call<User> call, Throwable t) {
        t.printStackTrace();
    }
}
