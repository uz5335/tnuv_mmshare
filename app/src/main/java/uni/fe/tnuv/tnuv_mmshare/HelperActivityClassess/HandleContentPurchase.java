package uni.fe.tnuv.tnuv_mmshare.HelperActivityClassess;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uni.fe.tnuv.tnuv_mmshare.MyFiles;
import uni.fe.tnuv.tnuv_mmshare.ApplicationObjectAndStorage.ApplicationObject;
import uni.fe.tnuv.tnuv_mmshare.ApplicationObjectAndStorage.PresheredStorageClass;
import uni.fe.tnuv.tnuv_mmshare.Login;
import uni.fe.tnuv.tnuv_mmshare.R;

public class HandleContentPurchase implements Callback<ResponseBody> {
    private final Context context;
    public HandleContentPurchase(Context context){
        this.context = context;
    }

    @Override
    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        if(response.isSuccessful()){
            //preusmeritev na pregled vseh datotek...
            Intent intent = new Intent(this.context, MyFiles.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.context.startActivity(intent);
        }else{
            JSONObject errorMessage;
            try {
                errorMessage = new JSONObject(response.errorBody().string());
                Toast.makeText(this.context, errorMessage.getString("error"), Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                //errorMessage = "An error occurred: error while decoding the error message.";
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Toast.makeText(this.context,this.context.getResources().getString(R.string.errorService), Toast.LENGTH_SHORT).show();
            final ApplicationObject appObject = (ApplicationObject) this.context;
            appObject.deleteUserId();
            PresheredStorageClass.deleteFromStorage(this.context);
            Intent intent = new Intent(this.context, Login.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.context.startActivity(intent);
        }
    }

    @Override
    public void onFailure(Call<ResponseBody> call, Throwable t) {
        t.printStackTrace();
    }
}
