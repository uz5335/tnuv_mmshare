package uni.fe.tnuv.tnuv_mmshare.Utils;

import android.content.Context;
import android.content.Intent;


public class IntentUtils {

    public static void StartNewActivity(Context context, Object className){
        Intent intent = new Intent(context, (Class<?>) className);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

    }

    public static void StartNewActivityWithUsernameAndCredits(Context context, Object className,
                                                              String u, String c){
        Intent intent = new Intent(context, (Class<?>) className);
        intent.putExtra("username", u);
        intent.putExtra("credits", c);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

    }
}
