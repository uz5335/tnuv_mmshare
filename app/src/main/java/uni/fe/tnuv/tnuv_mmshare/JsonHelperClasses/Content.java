package uni.fe.tnuv.tnuv_mmshare.JsonHelperClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Content {
    @SerializedName("access")
    @Expose
    private List<Access> access = null;
    @SerializedName("timeCreated")
    @Expose
    private String timeCreated;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("creator")
    @Expose
    private Creator creator;
    @SerializedName("topicID")
    @Expose
    private TopicID topicID;
    @SerializedName("fileName")
    @Expose
    private String fileName;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("mimetype")
    @Expose
    private String mimetype;

    public List<Access> getAccess() {
        return access;
    }

    public void setAccess(List<Access> access) {
        this.access = access;
    }

    public String getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(String timeCreated) {
        this.timeCreated = timeCreated;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Creator getCreator() {
        return creator;
    }

    public void setCreator(Creator creator) {
        this.creator = creator;
    }

    public TopicID getTopicID() {
        return topicID;
    }

    public void setTopicID(TopicID topicID) {
        this.topicID = topicID;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getMimetype() {
        return mimetype;
    }

    public void setMimetype(String mimetype) {
        this.mimetype = mimetype;
    }

}



