package uni.fe.tnuv.tnuv_mmshare.Fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import uni.fe.tnuv.tnuv_mmshare.R;


@SuppressLint("ValidFragment")
public class OwnFileFragment extends Fragment  {
    private FloatingActionButton download;
    private FloatingActionButton remove;
    @SuppressLint("ValidFragment")
    public OwnFileFragment(){

    }

    // The onCreateView method is called when Fragment should create its View object hierarchy,
    // either dynamically or via XML layout inflation.
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        View view = inflater.inflate(R.layout.fragment_own_file, parent, false);
        download = view.findViewById(R.id.downloadFAB);
        remove = view.findViewById(R.id.deleteFAB);


        //remove.setEnabled(false);
        return view;
    }

    // This event is triggered soon after onCreateView().
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        // Setup any handles to view objects here
        // EditText etFoo = (EditText) view.findViewById(R.id.etFoo);
        //
    }

}
