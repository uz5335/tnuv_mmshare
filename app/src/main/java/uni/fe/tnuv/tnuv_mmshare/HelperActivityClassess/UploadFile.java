package uni.fe.tnuv.tnuv_mmshare.HelperActivityClassess;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uni.fe.tnuv.tnuv_mmshare.ApplicationObjectAndStorage.ApplicationObject;
import uni.fe.tnuv.tnuv_mmshare.ApplicationObjectAndStorage.PresheredStorageClass;
import uni.fe.tnuv.tnuv_mmshare.Home;
import uni.fe.tnuv.tnuv_mmshare.Login;
import uni.fe.tnuv.tnuv_mmshare.R;

public class UploadFile implements Callback<ResponseBody> {
    private final Context context;
    private final ProgressBar progress;
    public UploadFile(Context context, ProgressBar progress){
        this.context = context;
        this.progress = progress;
    }

    @Override
    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        if(response.isSuccessful()){
            Log.d("jej", "UPLOAD SUCCESS");
            Toast toast = Toast.makeText(this.context, "File successfully uploaded", Toast.LENGTH_LONG);
            Intent intent = new Intent(this.context, Home.class);

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.context.startActivity(intent);

            toast.show();
            this.progress.setVisibility(View.GONE);
        }else{
            Log.w("tag", "NEEEEEEE");
            Toast.makeText(this.context, this.context.getResources().getString(R.string.errorService), Toast.LENGTH_SHORT).show();
            final ApplicationObject appObject = (ApplicationObject) this.context;
            appObject.deleteUserId();
            PresheredStorageClass.deleteFromStorage(this.context);
            Intent intent = new Intent(this.context, Login.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.context.startActivity(intent);
        }
    }
    @Override
    public void onFailure(Call<ResponseBody> call, Throwable t) {
        Log.e("tag", "something went terrible wrong");
    }
}
