package uni.fe.tnuv.tnuv_mmshare;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uni.fe.tnuv.tnuv_mmshare.ApplicationObjectAndStorage.ApplicationObject;
import uni.fe.tnuv.tnuv_mmshare.ApplicationObjectAndStorage.PresheredStorageClass;
import uni.fe.tnuv.tnuv_mmshare.HelperActivityClassess.HandleBalance;
import uni.fe.tnuv.tnuv_mmshare.HelperActivityClassess.HandleUser;
import uni.fe.tnuv.tnuv_mmshare.JsonHelperClasses.Topics;
import uni.fe.tnuv.tnuv_mmshare.PermisionsUtils.NetworkInfoClass;
import uni.fe.tnuv.tnuv_mmshare.Utils.DownloadsUtils;
import uni.fe.tnuv.tnuv_mmshare.Utils.IntentUtils;
import uni.fe.tnuv.tnuv_mmshare.Utils.ListTopicAdapter;
import uni.fe.tnuv.tnuv_mmshare.interfaces.SearchInterface;
import uni.fe.tnuv.tnuv_mmshare.interfaces.UserInterface;

public class Topic_list extends AppCompatActivity implements Callback<List<Topics>> {

    private ListTopicAdapter adapter;
    private SwipeRefreshLayout container;

    private DrawerLayout mDrawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle("Topics");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic_list);

        Toolbar app_bar = findViewById(R.id.app_bar);
        setSupportActionBar(app_bar);
        app_bar.setTitleTextColor(getResources().getColor(R.color.colorWhite));

        ActionBar actionbar = getSupportActionBar();
        Objects.requireNonNull(actionbar).setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);

        //setNavigationViewListener();


        mDrawerLayout = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);

        navigationView.bringToFront();

        View headerView = navigationView.getHeaderView(0);


        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        // set item as selected to persist highlight
                        //menuItem.setChecked(true);
                        // close drawer when item is tapped
                        mDrawerLayout.closeDrawers();

                        //mDrawerList.bringToFront();
                        //mDrawerLayout.requestLayout();

                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here
                        System.out.println("K L I K" + menuItem);
                        switch (menuItem.getItemId()) {

                            case R.id.nav_home: {
                                IntentUtils.StartNewActivity(getApplicationContext(), Home.class);
                                break;
                            }
                            case R.id.nav_files:
                                IntentUtils.StartNewActivity(getApplicationContext(), MyFiles.class);
                                break;

                            case R.id.nav_topics:
                                IntentUtils.StartNewActivity(getApplicationContext(), Topic_list.class);
                                break;

                            case R.id.nav_logout:
                                AlertDialog.Builder builder = new AlertDialog.Builder(Topic_list.this);
                                builder.setTitle("Do you really wanna LogOut?");

                                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        final ApplicationObject appObject = (ApplicationObject) getApplication();
                                        appObject.deleteUserId();
                                        PresheredStorageClass.deleteFromStorage(getApplicationContext());
                                        IntentUtils.StartNewActivity(getApplicationContext(), Login.class);
                                    }
                                });
                                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                //create dialog box...to show it..
                                AlertDialog ad = builder.create();
                                ad.show();
                                break;

                            case R.id.nav_downloads:
                                openDownloads();
                        }
                        return true;
                    }
                });


        NetworkInfo netInfo = NetworkInfoClass.getActiveNetworkInfo(getApplicationContext());
        if(netInfo == null || !netInfo.isConnected()){ //preverjanje ali sem kam povezan...
            Toast.makeText(Topic_list.this, "NO INTERNET CONNECTION", Toast.LENGTH_SHORT).show();
            final ApplicationObject appObject = (ApplicationObject) getApplication();
            appObject.deleteUserId();
            IntentUtils.StartNewActivity(getApplicationContext(), Login.class);
            return;
        }
        checkLogedInUser();
        ListView list = findViewById(R.id.topic_items);
        adapter = new ListTopicAdapter(getApplicationContext());

        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final Topics topics = adapter.getItem(i);
                if (topics != null) {
                    Log.d("tag", topics.getName());
                    final Intent intent = new Intent(Topic_list.this, Topic_search.class);
                    intent.putExtra(getResources().getString(R.string.topicName123), topics.getName());



                    startActivity(intent);
                }
            }
        });

        container = findViewById(R.id.container1);
        container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                SearchInterface.getInstance().allTopics().enqueue(Topic_list.this);
            }
        });

        TextView user = headerView.findViewById(R.id.drawerTitle);

        user.setText("LAlasfsdf");

        TextView credits = headerView.findViewById(R.id.drawerCredits);

        user.setText("LAlasfsdf");
        credits.setText("1234");

        System.out.println("userMENU " + user);

        //String text = getIntent().getStringExtra("username");
        //String text = getIntent().getStringExtra("credits");

        System.out.println("intent user" + getIntent().getStringExtra("username"));

        user.setText(getIntent().getStringExtra("username"));
        credits.setText(getIntent().getStringExtra("credits"));

        SearchInterface.getInstance().allTopics().enqueue(Topic_list.this);
        UserInterface.getInstance().GetOneUser(getUserId()).enqueue(new HandleUser(getApplicationContext(), user));
        UserInterface.getInstance().GetUserBalance(getUserId()).enqueue(new HandleBalance(getApplicationContext(), credits));





    }

    private void openDownloads(){
        DownloadsUtils.openDownloads(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkLogedInUser();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        checkLogedInUser();
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        checkLogedInUser();
    }

    private void  checkLogedInUser(){
        final ApplicationObject app = (ApplicationObject) getApplication();
        if(app.getUserID() == null){
            IntentUtils.StartNewActivity(getApplicationContext(), Login.class);
        }
    }

    /*

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        for(int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            SpannableString spanString = new SpannableString(menu.getItem(i).getTitle().toString());
            int end = spanString.length();
            spanString.setSpan(new RelativeSizeSpan(1.3f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            item.setTitle(spanString);
        }

        return true;
    }

    */

    public void showToast(String besedilo){

        Context context = getApplicationContext();
        String text =  besedilo; //getResources().getString(R.string.toast);
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

    /*
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.HOME:
                //showToast("going home"); //ukaz za pridobitev referenca hardcodana besedila.... v aplikaciji.
                IntentUtils.StartNewActivity(getApplicationContext(), Home.class);
                return true;
            case R.id.MYFILES:
                //showToast("My files");
                IntentUtils.StartNewActivity(getApplicationContext(), MyFiles.class);
                return true;
            case R.id.THEMES:
                //showToast("themes");
                return true;
            case R.id.LOGOUT:
                //showToast("logout");
                final ApplicationObject appObject = (ApplicationObject) getApplication();
                appObject.deleteUserId();
                PresheredStorageClass.deleteFromStorage(getApplicationContext());
                IntentUtils.StartNewActivity(getApplicationContext(), Login.class);
                return true;
            case R.id.DOWNLOADS:
                DownloadsUtils.openDownloads(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    */

    @Override
    public void onResponse(Call<List<Topics>> call, Response<List<Topics>> response) {
        List<Topics> topics = response.body();
        if(response.isSuccessful()){
            adapter.clear();
            adapter.addAll(topics);
        }else{
            Log.e("tag", "status not 2000");
        }
        container.setRefreshing(false);
    }

    @Override
    public void onFailure(Call<List<Topics>> call, Throwable t) {
        t.printStackTrace();
        container.setRefreshing(false);
    }

    private String getUserId() {
        final ApplicationObject app = (ApplicationObject) getApplication();
        return app.getUserID();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
