package uni.fe.tnuv.tnuv_mmshare;

import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import uni.fe.tnuv.tnuv_mmshare.ApplicationObjectAndStorage.ApplicationObject;
import uni.fe.tnuv.tnuv_mmshare.HelperActivityClassess.HandleLogin;
import uni.fe.tnuv.tnuv_mmshare.PermisionsUtils.NetworkInfoClass;
import uni.fe.tnuv.tnuv_mmshare.Utils.IntentUtils;
import uni.fe.tnuv.tnuv_mmshare.interfaces.UserInterface;

public class Login extends AppCompatActivity {
    private EditText username;
    private EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Button loginButton = findViewById(R.id.loginButton);
        loginButton.setOnClickListener(login);
        username = findViewById(R.id.usernameField);
        password = findViewById(R.id.passwordField);

    }

    @Override
    protected void onStart() {
        super.onStart();
        checkLogedInUser();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        checkLogedInUser();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkLogedInUser();
    }

    private final View.OnClickListener login = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //Log.i("TAG", getApplicationContext().getPackageName());
            NetworkInfo netInfo = NetworkInfoClass.getActiveNetworkInfo(getApplicationContext());
            if (netInfo == null || !netInfo.isConnected()) { //preverjanje ali sem kam povezan...
                Toast.makeText(Login.this, "NO INTERNET CONNECTION", Toast.LENGTH_SHORT).show();
            } else {
                if (username.getText().toString().trim().equals("") || password.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.loginErr), Toast.LENGTH_SHORT).show();
                } else {
                    HandleLogin login = new HandleLogin(getApplicationContext(), password, username);
                    UserInterface.getInstance().LoginCheck(username.getText().toString().trim(), password.getText().toString().trim()).enqueue(login);
                }
            }

        }
    };

    public void exitApp(View v) {
        finishAffinity();
        System.exit(0);
    }

    private void checkLogedInUser() {
        final ApplicationObject app = (ApplicationObject) getApplication();
        if (app.getUserID() != null) {
            IntentUtils.StartNewActivity(getApplicationContext(), Home.class);
        }
    }
}
