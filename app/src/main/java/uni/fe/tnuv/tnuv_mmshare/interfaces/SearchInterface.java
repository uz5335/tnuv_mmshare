package uni.fe.tnuv.tnuv_mmshare.interfaces;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import uni.fe.tnuv.tnuv_mmshare.JsonHelperClasses.Content;
import uni.fe.tnuv.tnuv_mmshare.JsonHelperClasses.Topics;

public class SearchInterface {


    public interface RestApi {
        String URL = "https://diploma-denar-uz5335.c9users.io/logicAPI/"; //da pridemo v locahost od windowsa...

        @GET("getAllTopics")
        Call<List<Topics>> allTopics ();

        @GET("searchByUserId/{userId}")
        Call<List<Content>> allContent (@Path("userId") String userId);

        @GET("getAllAccessTopic/{userId}")
        Call<List<Content>> allAccessContent (@Path("userId") String userId);

        @GET("searchContentInTopic/{topicName}")
        Call<List<Content>> searchTopics (@Path("topicName") String topicName);

    }

    private static RestApi instance;

    public static synchronized RestApi getInstance() {
        if (instance == null) {
            final Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(RestApi.URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            instance = retrofit.create(RestApi.class);
        }

        return instance;
    }


}
